package edu.ucsd.cs110w.temperature;

/**
 * @author dmozlows
 *
 */
public class Fahrenheit extends Temperature {

	public Fahrenheit(float v) {
		super(v);
	}
	
	public String toString() {
		return Float.toString(this.getValue()) + " F";
	}

	/* (non-Javadoc)
	 * @see com.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	@Override
	public Temperature toCelsius() {
		float val = (float) ((this.getValue() - 32)*(5.0/9));
		return new Celsius(val);
	}

	/* (non-Javadoc)
	 * @see com.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		return this;
	}

	@Override
	public Temperature toKelvin() {
		Temperature val = this.toCelsius();
		return val.toKelvin();
		
	}

}
