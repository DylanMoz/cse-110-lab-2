package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature {

	public Celsius(float v) {
		super(v);
	}

	public String toString() {
		return Float.toString(this.getValue()) + " C";
	}

	@Override
	public Temperature toCelsius() {
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		Float val = (float) ((this.getValue()*(9.0/5)) + 32);
		return new Fahrenheit(val);
	}

	@Override
	public Temperature toKelvin() {
		return new Kelvin(this.getValue() + 273);
	}

}
